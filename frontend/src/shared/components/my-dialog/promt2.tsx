import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import ClassService from '../../../service/class.service';

export default function AlertDialog(props: any) {

    const [disabled, setDisabled] = useState(false)
    const [saveText, setText] = useState('Đồng ý')
    const [allowClose, setAllowClose] = useState(true)
    const cService = new ClassService()
    const archive = (id: any) => {
        setDisabled(true);
        setText('Đang lưu trữ')
        setAllowClose(false)
        cService.archive({id: id}).subscribe(res => {
            console.log(res)
            setDisabled(false)
            setText('Đồng ý')
            setAllowClose(true)
            close()
            props.onRefresh()
        })
    }

    const close = () => {
        if(allowClose){
            props.handleClose();
        }
    }

  return (
    <div>
      <Dialog
        open={props.open}
        onClose={close}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{props.title}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {props.content}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={props.handleClose} color="primary">
            Hủy bỏ
          </Button>
          <Button disabled={disabled} onClick={() => {archive(props.archiveId); }} color="primary" autoFocus>
            {saveText}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
