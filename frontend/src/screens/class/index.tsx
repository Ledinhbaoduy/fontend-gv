import React, { Component } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import history from '../../navigation/App.history';
import MyCard from '../../shared/components/my-card';
import AuthorService from '../../service/authorized.service';
import ClassService from '../../service/class.service';
import './class.scss';
//import {MultiSkeleton} from './components/class-skeleton';
import {onLoading} from '../../redux/actionCreator/actionCreator';
import { APP_ROUTER } from '../../constants/router.constants';
import { Button } from '@material-ui/core';
import ClassDialog from './create-class-dialog';
import EditDialog from './components/edit-class-dialog';
import BoxContainer from '../../shared/components/box-container/box-container';
import MySelect from '../../shared/components/my-select';
import { DROPDOWN_CLASS } from '../../redux/actionTypes/actionTypes';
import {Dropdown} from '../../models/common.model';
import Promt from '../../shared/components/my-dialog/promt2';

const ShowOptions : Dropdown[] = [ 
    {
        id: 'onlyactive',
        value: 'Chỉ những lớp đang hoạt động'
    },
    {
        id: 'all',
        value: 'Tất cả lớp'
    },
    {
        id: 'onlyarchive',
        value: 'Chỉ những lớp đã được lưu trữ'
    }  
]

interface IClassProps {
    logged?:boolean;
    isLoading?: boolean;
    onLoading?:any;
}
interface IClassState {
    items: any,
    isShow: boolean,
    isEShow: boolean,
    isAShow: boolean,
    c_id: string,
    title: string,
    value: string,
    sources: any,
    active: any,
    archive: any,
    archiveId: any,
}

class ClassScreen extends Component<IClassProps, IClassState> {
    private author = new AuthorService();
    private cService = new ClassService();
    constructor(props: IClassProps){
        super(props);
        this.state = {
            items: [],
            isShow: false,
            isEShow: false,
            isAShow: false,
            c_id: '',
            title: '',
            value: 'onlyactive',
            sources: [],
            active: [],
            archive: [],
            archiveId: '',
        }
        this.editInfo = this.editInfo.bind(this);
        this.onArchive = this.onArchive.bind(this)
    }

    componentDidMount(){
        this.props.onLoading(true)
        const id = this.author.getUserInfo().user_info ? this.author.getUserInfo().user_info.id : '';
        if(id == ''){
            history.push('/')
        }
        this.cService.getAllClassesByTeacher(id).subscribe(res=>{
            this.setState({sources: res});
            this.props.onLoading(false);
            this.getOnlyActiveClasses()
            this.getOnlyArchiveClasses()
            this.selectedChange('onlyactive')
            document.title= 'Lớp học'
        })
    }

    componentDidUpdate(){
        if(!this.author.getUserInfo().user_info.id){
            history.push(APP_ROUTER.LOGIN.HOME)
        }
        else{
            
        }
    }

    onClose = () => {
        this.setState({isShow: false});
    }

    onEClose = () => {
        this.setState({isEShow: false});
    }

    onAClose = () => {
        this.setState({isAShow: false});
    }

    onRefresh = () => {
        this.props.onLoading(true)
        this.cService.getAllClassesByTeacher(this.author.getIdUsers()).subscribe(res => {
            this.setState({items: this.state.active})
            this.props.onLoading(false)
        })
    }

    editInfo = (id:any, title:any) => {
        this.setState({isEShow: true, c_id: id, title: title})
    }

    getOnlyActiveClasses = () => {
        this.setState({active: []})
        this.state.sources.map((x:any) => {
            if(x.status){
                this.state.active.push(x)
            }
        })
        //this.setState({items: this.state.active})
    }

    getOnlyArchiveClasses = () =>{
       this.setState({archive: []})
       this.state.sources.map((x:any) => {
           if(!x.status){
               this.state.archive.push(x)
           }
       })
       //this.setState({items: this.state.archive})
    }


    selectedChange = (e: string) => {
        switch(e){
            case 'onlyactive':
                this.setState({items: this.state.active})
                break;
            case 'all':
                this.setState({items: this.state.sources})
                break;
            default:
                this.setState({items: this.state.archive})
                break;
        }
        this.setState({value: e})
    }
    
    onArchive = (id:any) => {
        this.setState({isAShow: true, archiveId: id})
    }
    render(){ 
        return (
            <div className='container'>
                <Promt open={this.state.isAShow} handleClose={this.onAClose} title='Lưu trữ lớp' content='Bạn có chắc chắn muốn lưu trữ lớp?' onRefresh={this.onRefresh} archiveId={this.state.archiveId}/>
                <Button style={{marginTop:'1%', marginRight:'1%'}} variant='contained' color='primary' onClick={()=> this.setState({isShow: true})}>Tạo lớp</Button>
                <MySelect items={ShowOptions} label='Hiển thị' onChange={this.selectedChange} value={this.state.value} />
                <div className='contain'>
                {
                !this.props.isLoading 
                ? this.state.items ? this.state.items
                .map((x:any)=><MyCard status={x.status} editInfo={this.editInfo} archive={this.onArchive} id={x._id} title={x.value} description={x.des ? x.des : ''}/> ) : ''
                : ''}
                </div>
                
                <ClassDialog onRefreshTable={this.onRefresh} handleClose={this.onClose} open={this.state.isShow} typeName='Tạo lớp học' />
                <EditDialog id={this.state.c_id} title={this.state.title} onRefreshTable={this.onRefresh} handleClose={this.onEClose} open={this.state.isEShow} typeName='Sửa thông tin' />
            </div>
        )
    }
}



const mapStateToProps = (state: any) => {
    return {
        logged: state.data.logged,
        isLoading: state.data.isLoading,
    }
}

const mapDispatchToProps = (dispatch: any) => {
    const actionCreator = { onLoading };
    const action = bindActionCreators(actionCreator, dispatch);
    return {...action};
}

export default connect(mapStateToProps, mapDispatchToProps)(ClassScreen);